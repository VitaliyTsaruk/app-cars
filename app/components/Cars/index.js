require('../Tabs');

angular.module('Cars', ['tabs'])
    .component('cars', {
        templateUrl: '../components/Cars/cars.html',
        controller: CarsComponent,
        controllerAs: 'ctrl'
    });


function CarsComponent () {

    this.isForm = false;
    this.cars = [];

    var currenYear = new Date().getFullYear().toString();
    var years = currenYear.split('')[2];
    var years2 = currenYear.split('')[3];
    this.yearRegEx = new RegExp('^(19[5-9][0-9]|20[0-' + (years - 1) +'][0-9]|20[0-' + years +'][0-'+ years2 +'])$');

    this.addCar = function () {
        this.cars.push(this.car);
        this.clearCar();
    }

    this.clearCar = function () {
        this.car = {};
        this.index = undefined;
        this.isUpdate = undefined;
    }

    this.updateCar = function () {

        if (this.checkIsChange())

            return alert("Is not change!");

        this.cars[this.index] = this.car;
        this.clearCar();
    }

    this.changeCar = function (car, index) {

        this.index = index;
        this.isUpdate = true;
        this.isForm = true;
        this.car = {
            name: car.name,
            model: car.model,
            year: car.year,
            type: car.type,
            vehiclId: car.vehiclId
        }
    }

    this.showForm = function () {
        this.clearCar();
        this.isForm = true;
    }

    this.checkIsChange = function () {

        if (typeof this.index === 'indefined' ) return;

        for (var key in this.car) {

            if (this.car[key] !== this.cars[this.index][key])

                return false;
        }

        return true;
    }
}

CarsComponent.$injector = [];

